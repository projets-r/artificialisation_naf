library(glue)
library(here)

get_data_from_gitlab <- function(project_id, branch_name, filepath) {
  filename <- basename(filepath)
  filepath <- URLencode(filepath, reserved = TRUE)
  url <- glue("https://gitlab.com/api/v4/projects/{project_id}")
  url <- glue("{url}/repository/files/{filepath}/raw?ref={branch_name}")
  dest_folder <- "data-raw"
  dest_file <- here(dest_folder, filename)
  dir.exists(dest_folder) || dir.create(dest_folder)
  if (!file.exists(dest_file)) {
    download.file(url = url, destfile = dest_file)
  }
}


